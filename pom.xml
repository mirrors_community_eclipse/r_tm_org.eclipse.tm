<?xml version="1.0" encoding="UTF-8"?>
<!--
    Copyright (c) 2018 Red Hat and others.
    This program and the accompanying materials
    are made available under the terms of the Eclipse Public License 2.0
    which accompanies this distribution, and is available at
    https://www.eclipse.org/legal/epl-2.0/
   
    SPDX-License-Identifier: EPL-2.0
 -->

<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<modelVersion>4.0.0</modelVersion>

	<groupId>org.eclipse.tm</groupId>
	<artifactId>tm-parent</artifactId>
	<version>4.5.700</version>
	<packaging>pom</packaging>
	<name>Target Management Parent</name>

	<properties>
		<tycho-version>4.0.8</tycho-version>
		<tycho-extras-version>${tycho-version}</tycho-extras-version>
		
		<tycho.scmUrl>scm:git:git://git.eclipse.org/gitroot/tm/org.eclipse.tm.git</tycho.scmUrl>
		<jgitDirtyWorkingTree>error</jgitDirtyWorkingTree>
		
		<cbi-plugins.version>1.3.2</cbi-plugins.version>

		<maven-resources-version>3.3.1</maven-resources-version>
		<maven-antrun-version>3.1.0</maven-antrun-version>
		<gmaven-version>1.5</gmaven-version>
		<findbugs-version>3.0.4</findbugs-version>
		<!-- Check available versions at https://repository.sonatype.org/content/repositories/public/org/jacoco/jacoco-maven-plugin -->
		<jacoco-version>0.8.8</jacoco-version>

		<target-platform>eclipse-4.32</target-platform>
		<adminDir>${env.WORKSPACE}/admin</adminDir>
	</properties>

	<pluginRepositories>
		<pluginRepository>
			<id>sonatype</id>
			<url>http://oss.sonatype.org/content/repositories/public</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<releases>
				<enabled>true</enabled>
			</releases>
		</pluginRepository>
		<pluginRepository>
			<id>cbi</id>
			<url>https://repo.eclipse.org/content/repositories/cbi-releases/</url>
		</pluginRepository>
		<pluginRepository>
			<id>cbi-snapshots</id>
			<url>https://repo.eclipse.org/content/repositories/cbi-snapshots/</url>
		</pluginRepository>
	</pluginRepositories>
	<build>
		<plugins>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-maven-plugin</artifactId>
				<version>${tycho-version}</version>
				<extensions>true</extensions>
			</plugin>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>target-platform-configuration</artifactId>
				<version>${tycho-version}</version>
				<configuration>
					<executionEnvironment>JavaSE-17</executionEnvironment>
					<environments>
						<environment>
							<os>linux</os>
							<ws>gtk</ws>
							<arch>x86_64</arch>
						</environment>
					</environments>
					<target>
						<file>${adminDir}/target-defs/${target-platform}.target</file>
					</target>
				</configuration>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-compiler-plugin</artifactId>
					<version>${tycho-version}</version>
					<configuration>
						<compilerArgument>-warn:+discouraged,forbidden</compilerArgument>
						<!-- ignore project settings which appear in Eclipse but are ignored by Tycho <= 0.26, like API access restrictions -->
						<!-- <useProjectSettings>false</useProjectSettings> -->
						<encoding>UTF-8</encoding>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-source-plugin</artifactId>
					<version>${tycho-version}</version>
					<configuration></configuration>
					<executions>
						<execution>
							<id>plugin-source</id>
							<goals>
								<goal>plugin-source</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-packaging-plugin</artifactId>
					<version>${tycho-version}</version>
					<dependencies>
						<dependency>
							<groupId>org.eclipse.tycho</groupId>
							<artifactId>tycho-buildtimestamp-jgit</artifactId>
							<version>${tycho-extras-version}</version>
						</dependency>
						<dependency>
							<groupId>org.eclipse.tycho.extras</groupId>
							<artifactId>tycho-sourceref-jgit</artifactId>
							<version>${tycho-extras-version}</version>
						</dependency>
					</dependencies>
					<configuration>
						<jgit.dirtyWorkingTree>${jgitDirtyWorkingTree}</jgit.dirtyWorkingTree>
						<strictBinIncludes>false</strictBinIncludes>
						<timestampProvider>jgit</timestampProvider>
						<sourceReferences>
							<generate>true</generate>
						</sourceReferences>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-p2-repository-plugin</artifactId>
					<version>${tycho-version}</version>
					<configuration>
						<finalName>${project.artifactId}</finalName>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-resources-plugin</artifactId>
					<version>${maven-resources-version}</version>
					<configuration>
						<encoding>UTF-8</encoding>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-antrun-plugin</artifactId>
					<version>${maven-antrun-version}</version>
				</plugin>
				<plugin>
					<groupId>org.codehaus.gmaven</groupId>
					<artifactId>gmaven-plugin</artifactId>
					<version>${gmaven-version}</version>
				</plugin>
				<plugin>
					<groupId>org.codehaus.mojo</groupId>
					<artifactId>findbugs-maven-plugin</artifactId>
					<version>${findbugs-version}</version>
					<configuration>
						<encoding>UTF-8</encoding>
						<effort>Max</effort>
						<threshold>Low</threshold>
						<xmlOutput>true</xmlOutput>
						<failOnError>false</failOnError>
						<excludeFilterFile>${adminDir}/findbugs-exclude.xml</excludeFilterFile>
					</configuration>
					<executions>
						<execution>
							<goals>
								<goal>check</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.jacoco</groupId>
					<artifactId>jacoco-maven-plugin</artifactId>
					<version>${jacoco-version}</version>
				</plugin>
				<plugin>
					<groupId>org.eclipse.tycho</groupId>
					<artifactId>tycho-surefire-plugin</artifactId>
					<version>${tycho-version}</version>
					<configuration>
						<!-- <useUIHarness>true</useUIHarness>  -->
						<!-- <useUIThread>true</useUIThread> -->
						<failIfNoTests>false</failIfNoTests>
						<product>org.eclipse.platform.ide</product>
						<!-- <argLine>-Xms512m -Xmx1024m -XX:PermSize=256m -XX:MaxPermSize=256m -Drse.enableSecureStoreAccess=false -Dorg.eclipse.swt.browser.UseWebKitGTK=true</argLine> -->
						<argLine> -ea </argLine>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<profiles>
		<profile>
			<id>eclipse-sign</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.eclipse.cbi.maven.plugins</groupId>
						<artifactId>eclipse-jarsigner-plugin</artifactId>
						<version>${cbi-plugins.version}</version>
						<executions>
							<execution>
								<id>sign</id>
								<goals>
									<goal>sign</goal>
								</goals>
								<phase>verify</phase>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>org.eclipse.tycho</groupId>
						<artifactId>tycho-p2-plugin</artifactId>
						<version>${tycho-version}</version>
						<executions>
							<execution>
								<id>p2-metadata</id>
								<goals>
									<goal>p2-metadata</goal>
								</goals>
								<phase>package</phase>
							</execution>
						</executions>
						<configuration>
							<defaultP2Metadata>false</defaultP2Metadata>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

	<modules>
		<!-- RSE features and plug-ins -->
		<module>terminal</module>
		<module>rse</module>
		<module>site</module>
	</modules>
</project>