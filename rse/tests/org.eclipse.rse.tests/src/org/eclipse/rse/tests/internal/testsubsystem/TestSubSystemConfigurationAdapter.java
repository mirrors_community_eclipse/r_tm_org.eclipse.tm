/*******************************************************************************
 * Copyright (c) 2006, 2007 Wind River Systems, Inc. and others.
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License 2.0 
 * which accompanies this distribution, and is available at 
 * https://www.eclipse.org/legal/epl-2.0/ 
 * 
 * Contributors: 
 * Tobias Schwarz (Wind River) - initial API and implementation
 *******************************************************************************/
package org.eclipse.rse.tests.internal.testsubsystem;

import org.eclipse.rse.ui.view.SubSystemConfigurationAdapter;

/**
 * Adapter for subsytsem configuration.
 */
public class TestSubSystemConfigurationAdapter extends SubSystemConfigurationAdapter {

	/**
	 * Constructor.
	 */
	public TestSubSystemConfigurationAdapter() {
		super();
	}
}
